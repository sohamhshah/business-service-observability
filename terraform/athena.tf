## Create Glue Catalog Database
resource "aws_glue_catalog_database" "app_logs_db" {
  name = "app_logs_db"
}

## Create Glue Table
resource "aws_glue_catalog_table" "istio_logs" {
  name          = "istio_logs"
  database_name = aws_glue_catalog_database.app_logs_db.name
  description   = "AWS Glue Catalog Table for Istio Logs"
  table_type    = "EXTERNAL_TABLE"

  storage_descriptor {
    location      = "s3://${aws_s3_bucket.athena_source_bucket.bucket}/logs/"
    input_format  = "org.apache.hadoop.mapred.TextInputFormat"
    output_format = "org.apache.hadoop.hive.ql.io.IgnoreKeyTextOutputFormat"


    ser_de_info {
      name                  = "s3-stream"
      serialization_library = "org.openx.data.jsonserde.JsonSerDe"


      parameters = {
        "ignore.malformed.json" = "TRUE"
        "dots.in.keys"          = "FALSE"
        "case.insensitive"      = "TRUE"
        "mapping"               = "TRUE"
      }
    }


    dynamic "columns" {
      for_each = var.columns


      iterator = column
      content {
        name = column.value.name
        type = column.value.type
      }
    }
  }
}

## Create Athena WorkGroup
resource "aws_athena_workgroup" "app_logs_wg" {
  name = "app_logs_wg"

  configuration {
    enforce_workgroup_configuration    = true
    publish_cloudwatch_metrics_enabled = true

    result_configuration {
      output_location = "s3://${aws_s3_bucket.athena_result_bucket.bucket}/output/"

    }
  }
}

## Create Athena Named Query
resource "aws_athena_named_query" "istio_logs_query" {
  name      = "istio_logs_query"
  workgroup = aws_athena_workgroup.app_logs_wg.id
  database  = aws_glue_catalog_database.app_logs_db.name
  query     = "SELECT json_extract_scalar(person, '$.name') as name, product as source FROM app_logs_db.istio_logs;"
}

## Place logs.json in S3
resource "aws_s3_bucket_object" "earth_service_app_log_file_001" {
  bucket = aws_s3_bucket.athena_source_bucket.bucket
  key    = "logs/earth-service/2024/05/16/17/logs.json"
  source = "./resources/earth-service/logs.json"
}

resource "aws_s3_bucket_object" "jupiter_service_app_log_file_001" {
  bucket = aws_s3_bucket.athena_source_bucket.bucket
  key    = "logs/jupiter-service/2024/05/16/17/logs.json"
  source = "./resources/jupiter-service/logs.json"
}

resource "aws_s3_bucket_object" "jupiter_service_app_log_file_001" {
  bucket = aws_s3_bucket.athena_source_bucket.bucket
  key    = "logs/jupiter-service/2024/05/16/17/logs.json"
  source = "./resources/jupiter-service/logs.json"
}