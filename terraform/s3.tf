## Random Resource
resource "random_id" "id" {
  byte_length = 8
}

## Create S3 Bucket for Raw Data
resource "aws_s3_bucket" "athena_source_bucket" {
  bucket = "athena-source-bucket-${random_id.id.hex}"
}

## Create S3 Bucket for Results
resource "aws_s3_bucket" "athena_result_bucket" {
  bucket = "athena-result-bucket-${random_id.id.hex}"
}